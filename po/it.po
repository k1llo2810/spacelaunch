# Italian translations for spacelaunch package.
# Copyright (C) 2022 THE spacelaunch'S COPYRIGHT HOLDER
# This file is distributed under the same license as the spacelaunch package.
# Automatically generated, 2022.
# Albano Battistella <albano_battistella@hotmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: spacelaunch\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-30 11:05+0200\n"
"PO-Revision-Date: 2022-07-29 14:53+0200\n"
"Last-Translator: Albano Battistella <albano_battistella@hotmail.com>\n"
"Language-Team: Italian <albano_battistella@hotmail.com>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 42.0\n"

#: data/org.emilien.SpaceLaunch.desktop.in:3
msgid "Space Launch"
msgstr "Space Launch"

#: src/ui/window.ui:17
msgid "Preferences"
msgstr "Preferenze"

#: src/ui/window.ui:21
msgid "About Space Launch"
msgstr "Informazioni su Space Launch"

#: src/ui/window.ui:113
msgid "Refresh database"
msgstr "RAggiorna database"

#: src/ui/window.ui:126
msgid "Back"
msgstr "Indietro"

#: src/ui/window.ui:135
msgid "Menu"
msgstr "Menu"

#: src/ui/pageVueNormale.ui:41
msgid "See more launches"
msgstr "Vedi altri lanci"

#: src/ui/pageVueCompacte.ui:64
msgid "See more launches..."
msgstr "Vedi altri lanci..."

#: src/ui/pageVueLancement.ui:147
msgid "Launch date"
msgstr "Data di lancia"

#: src/ui/pageVueLancement.ui:166
msgid "Launch window"
msgstr "Finestra di lancio"

#: src/ui/pageVueLancement.ui:186
msgid "Launch pad"
msgstr "Piattaforma di lancio"

#: src/ui/pageVueLancement.ui:220
msgid "Overview"
msgstr "Presentazione"

#: src/ui/pageVueLancement.ui:239
msgid "Destination:"
msgstr "Destinazione :"

#: src/ui/pageVueLancement.ui:249
msgid "Mission:"
msgstr "Missione :"

#: src/ui/pageReglages.ui:20
msgid "Reset filters"
msgstr "Reimposta filtri"

#: src/ui/pageReglages.ui:33
msgid "Filters"
msgstr "Filtri"

#: src/ui/pageReglages.ui:37
msgid "Launch countries"
msgstr "Paesi di lancio"

#: src/ui/pageReglages.ui:41
msgid "China"
msgstr "Cina"

#: src/ui/pageReglages.ui:43
msgid "Taiyuan, Jiuquan, Xichang, Wenchang"
msgstr "Taiyuan, Jiuquan, Xichang, Wenchang"

#: src/ui/pageReglages.ui:59
msgid "France | French Guiana"
msgstr "Francia | Guinea Francese"

#: src/ui/pageReglages.ui:61
msgid "Kourou"
msgstr "Kourou"

#: src/ui/pageReglages.ui:77
msgid "India"
msgstr "India"

#: src/ui/pageReglages.ui:79
msgid "Sriharikota"
msgstr "Sriharikota"

#: src/ui/pageReglages.ui:95
msgid "Japan"
msgstr "Giappone"

#: src/ui/pageReglages.ui:97
msgid "Tanegashima, Uchinoura Space Center, Taiki-cho"
msgstr "Tanegashima, Uchinoura Space Center, Taiki-cho"

#: src/ui/pageReglages.ui:113
msgid "Kazakhstan"
msgstr "Kazakhstan"

#: src/ui/pageReglages.ui:115
msgid "Baikonour"
msgstr "Baikonour"

#: src/ui/pageReglages.ui:131
msgid "New Zealand"
msgstr "Nuova Zelanda"

#: src/ui/pageReglages.ui:133
msgid "Onenui Station"
msgstr "Onenui Station"

#: src/ui/pageReglages.ui:149
msgid "Russia"
msgstr "Russia"

#: src/ui/pageReglages.ui:151
msgid "Vostochny, Plesetsk"
msgstr "Vostochny, Plesetsk"

#: src/ui/pageReglages.ui:167
msgid "United States of America"
msgstr "Stati Uniti"

#: src/ui/pageReglages.ui:169
msgid "Cape Canaveral, Kennedy Space Center, Wallops Island, Vandenberg, ..."
msgstr "Cape Canaveral, Kennedy Space Center, Wallops Island, Vandenberg, ..."

#: src/ui/pageReglages.ui:185
msgid "Others"
msgstr "Altri"

#: src/ui/pageReglages.ui:206
msgid "General"
msgstr "Generale"

#: src/ui/pageReglages.ui:210
msgid "System settings"
msgstr "Impostazioni di sistema"

#: src/ui/pageReglages.ui:213
msgid "Embed video stream"
msgstr "Incorpora flusso video"

#: src/ui/pageReglages.ui:215
msgid "If possible, video stream will be open in application"
msgstr "Se possibile, il flusso video sarà aperto nell'applicazione"

#: src/ui/pageReglages.ui:231
msgid "Cache size"
msgstr "Dimensione della cache"

#: src/ui/menuTheme.ui:19 src/ui/menuTheme.ui:21
msgid "Follow system style"
msgstr "Applica il tema del sistema"

#: src/ui/menuTheme.ui:34 src/ui/menuTheme.ui:36
msgid "Light style"
msgstr "Tema chiaro"

#: src/ui/menuTheme.ui:50 src/ui/menuTheme.ui:52
msgid "Dark style"
msgstr "Tema scuro"

#: src/application.vala:72
msgid "translator-credits"
msgstr "Albano Battistella <albano_battistella@hotmail.com>"

#. POUR FAIRE APPARAÎTRE LE BOUTON PLUS SI LA VUE LE PERMET
#: src/window.vala:71 src/window.vala:152 src/window.vala:170
msgid "Upcoming launches"
msgstr "Prossimi lanci"

#: src/window.vala:215
msgid "Loading database..."
msgstr "Caricamento del database..."

#. ERREUR INTERNET
#: src/window.vala:230
msgid "Check your internet connection!"
msgstr "Controlla la tua connessione internet!"

#: src/window.vala:265 src/window.vala:299
msgid "Loading additionnal launches..."
msgstr "Caricamento di altri lanci..."

#: src/window.vala:341
msgid "Loading launch..."
msgstr "Caricamento del lancio..."

#: src/pageReglages.vala:122
msgid "Are you sure you want to clear the cache folder?"
msgstr "Sei sicuro di voler svuotare la cartella della cache?"

#: src/pageReglages.vala:124
msgid "Cancel"
msgstr "Cancella"

#: src/pageReglages.vala:126
msgid "Delete"
msgstr "Elimina"

#: src/pageReglages.vala:148
msgid "Because of limitations of GTK4, this feature is currently not available"
msgstr ""
"A causa delle limitazioni di GTK4, questa funzionalità non è attualmente "
"disponibile"
