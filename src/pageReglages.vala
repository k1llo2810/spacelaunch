namespace Spacelaunch {
    [GtkTemplate (ui = "/org/emilien/SpaceLaunch/ui/pageReglages.ui")]
    public class Reglages : Adw.PreferencesWindow {

        [GtkChild]
        public unowned Gtk.Switch switchFiltreCHN;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreFRA;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreIND;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreJPN;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreKAZ;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreNZL;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreRUS;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreUSA;
        [GtkChild]
        public unowned Gtk.Switch switchFiltreAutres;
        [GtkChild]
        public unowned Gtk.Button boutonReset;
        [GtkChild]
        public unowned Gtk.Switch switchEmbed;
        [GtkChild]
        public unowned Adw.ActionRow actionCache;
        [GtkChild]
        public unowned Gtk.Button boutonSupprimerCache;

        public Window win {get; set;}

        private bool[] initial;

        public Reglages (Window win) {
            Object (
                win: win
            );
            transient_for = win;
            show ();

            initial = creationListeFiltres();
            visibliteBouton();

            switchFiltreCHN.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrechn", true); visibliteBouton(); });
            switchFiltreFRA.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrefra", true); visibliteBouton(); });
            switchFiltreIND.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtreind", true); visibliteBouton(); });
            switchFiltreJPN.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrejpn", true); visibliteBouton(); });
            switchFiltreKAZ.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrekaz", true); visibliteBouton(); });
            switchFiltreNZL.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrenzl", true); visibliteBouton(); });
            switchFiltreRUS.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtrerus", true); visibliteBouton(); });
            switchFiltreUSA.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtreusa", true); visibliteBouton(); });
            switchFiltreAutres.notify["active"].connect(() => { if(!verifFiltres()[0]) filtres.set_boolean("filtreautres", true); visibliteBouton(); });
            switchEmbed.notify["active"].connect(() => { if(switchEmbed.get_active()) avertissement(); });

            boutonReset.clicked.connect( () => {if(!verifFiltres()[1]) reinitialisationFiltres();});

            calculCache();
            boutonSupprimerCache.clicked.connect(effacementCache);

            this.close_request.connect( () => {chgtFiltre();return false;});
        }

        construct {
            filtres.bind ("filtrechn",
                switchFiltreCHN, "active",
                DEFAULT
            );
            filtres.bind ("filtrefra",
                switchFiltreFRA, "active",
                DEFAULT
            );
            filtres.bind ("filtreind",
                switchFiltreIND, "active",
                DEFAULT
            );
            filtres.bind ("filtrejpn",
                switchFiltreJPN, "active",
                DEFAULT
            );
            filtres.bind ("filtrekaz",
                switchFiltreKAZ, "active",
                DEFAULT
            );
            filtres.bind ("filtrenzl",
                switchFiltreNZL, "active",
                DEFAULT
            );
            filtres.bind ("filtrerus",
                switchFiltreRUS, "active",
                DEFAULT
            );
            filtres.bind ("filtreusa",
                switchFiltreUSA, "active",
                DEFAULT
            );
            filtres.bind ("filtreautres",
                switchFiltreAutres, "active",
                DEFAULT
            );
            settings.bind ("embed",
                switchEmbed, "active",
                DEFAULT
            );
        }

        public void calculCache() {

            string taille, erreur;
            int status;
            Process.spawn_command_line_sync ("du -ksh "+ dossierCache, out taille, out erreur, out status);

            int delimiteur =  taille.index_of_char('/');
            taille = taille.substring(0,delimiteur-1);
            taille += "B";
            actionCache.set_subtitle(taille);
        }

        private void effacementCache() {

            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Are you sure you want to clear the cache folder?"),null);
            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));
            dialog.add_response("delete", _("Delete"));
            dialog.set_response_appearance("delete", Adw.ResponseAppearance.DESTRUCTIVE);

            dialog.response.connect((response) => {
                if(response == "delete"){
                    Dir dir = Dir.open (dossierCache, 0);
                    string? name = null;
                    while ((name = dir.read_name ()) != null) {
                        string path = Path.build_filename (dossierCache, name);
                        if (FileUtils.test (path, FileTest.IS_REGULAR)) {
                            FileUtils.remove(path);
                        }
                    }
                calculCache();
                }
            });

            dialog.present();
        }

        private void avertissement() {

            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Warning:"), _("Because of limitations of GTK4, this feature is currently not available"));
            dialog.add_response("ok", _("OK"));
            dialog.present();
            switchEmbed.set_state(false);
        }

        private void visibliteBouton() {

            if(verifFiltres()[1]) {
                boutonReset.get_style_context().remove_class("suggested-action");
            }
            else {
                boutonReset.get_style_context().add_class("suggested-action");
            }

        }

        private void chgtFiltre() {

            bool[] listeFiltres = creationListeFiltres();
            bool chgt = false;

            for(int i=0;i<listeFiltres.length;i++){
                if(listeFiltres[i] != initial[i]) chgt = true;
            }

            if(chgt) GLib.Timeout.add(100, () => {win.chgtFiltre();return false;});
        }
    }

    public bool[] creationListeFiltres() {

        bool[] liste = null;

        liste += filtres.get_boolean("filtrechn");
        liste += filtres.get_boolean("filtrefra");
        liste += filtres.get_boolean("filtreind");
        liste += filtres.get_boolean("filtrejpn");
        liste += filtres.get_boolean("filtrekaz");
        liste += filtres.get_boolean("filtrenzl");
        liste += filtres.get_boolean("filtrerus");
        liste += filtres.get_boolean("filtreusa");
        liste += filtres.get_boolean("filtreautres");

        return liste;
    }

    public bool[] verifFiltres() {

        bool[] ok = {false,true};
        // ok[0] : false si tous les filtres sont désactivés
        // ok[1] : true si tous les filtres sont activés

        if(filtres.get_boolean("filtrechn")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtrefra")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtreind")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtrejpn")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtrekaz")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtrenzl")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtrerus")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtreusa")) {ok[0] = true;} else { ok[1] = false;};
        if(filtres.get_boolean("filtreautres")) {ok[0] = true;} else { ok[1] = false;};

        return ok;
    }

    public void reinitialisationFiltres() {

        filtres.set_boolean("filtrechn", true);
        filtres.set_boolean("filtrefra", true);
        filtres.set_boolean("filtreind", true);
        filtres.set_boolean("filtrejpn", true);
        filtres.set_boolean("filtrekaz", true);
        filtres.set_boolean("filtrenzl", true);
        filtres.set_boolean("filtrerus", true);
        filtres.set_boolean("filtreusa", true);
        filtres.set_boolean("filtreautres", true);
    }
}
