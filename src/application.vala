/* application.vala
 *
 * Copyright 2022 Emilien Lescoute
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Spacelaunch {
    public GLib.Settings settings;
    public GLib.Settings filtres;
    public string dossierConfig;
    public string dossierCache;

    public class Application : Adw.Application {

        public Application () {
            Object (application_id: "org.emilien.SpaceLaunch", flags: ApplicationFlags.FLAGS_NONE);
            settings = new GLib.Settings (Config.APP_ID);
            filtres = new GLib.Settings (Config.APP_ID);

            dossierConfig = GLib.Environment.get_user_config_dir()+"/SpaceLaunch";
            dossierCache = GLib.Environment.get_user_cache_dir()+"/SpaceLaunch";
            if(!GLib.FileUtils.test(dossierConfig, GLib.FileTest.IS_DIR)) DirUtils.create(dossierConfig,0700);
            if(!GLib.FileUtils.test(dossierCache, GLib.FileTest.IS_DIR)) DirUtils.create(dossierCache,0700);
        }

        construct {
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                //{ "preferences", on_preferences_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) {
                win = new Spacelaunch.Window (this);
            }
            win.present ();
        }

        private void on_about_action () {
            string[] developers = { "Emilien Lescoute <emilien.lescoute@lilo.org>",
                                        null };
            var program_name = "Space Launch";

            var about = new Adw.AboutWindow () {
                application_icon = Config.APP_ID,
                application_name = program_name,
                copyright = "© 2020-2023 Emilien Lescoute",
                comments = _("Space Launch is a native unofficial SpaceLaunchNow.me client for GNOME, written in Vala and GTK4. It allows to keep track of upcoming rocket launches."),
                developer_name = "Emilien Lescoute",
                developers = developers,
                issue_url = "https://gitlab.com/elescoute/spacelaunch/issues",
                license_type = GPL_3_0,
                transient_for = this.active_window,
                // translators: Write your name<email> here :D
                translator_credits = _("translator_credits"),
                version = Config.VERSION,
                website = "https://gitlab.com/elescoute/spacelaunch",
            };

            about.present ();
        }
    }
}
