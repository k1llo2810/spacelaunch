namespace  Spacelaunch{

string lectureBloc(string motCle, string bloc){

    string sousBloc = "";

    bool debutBloc = false;
    bool finBloc = false;
    int acc=0;
    int debut=0;

    for(int i = 0; i < bloc.length; i++){
        if(i < (bloc.length - motCle.length -4) && bloc.substring(i,motCle.length+4) == "\""+motCle+"\":{" && !debutBloc){
            debut = i + motCle.length + 4;
            debutBloc = true;
        }
        if(debutBloc && !finBloc && bloc.substring(i,1)=="{") acc++;
        if(debutBloc && !finBloc && bloc.substring(i,1)=="}"){
            acc--;
            if(acc==0){
                finBloc = true;
                sousBloc = bloc.substring(debut,i-debut+1);
            }
        }
    }

    return sousBloc;
}

string lectureBloc2(string motCle, string bloc){

    string sousBloc = "";

    bool debutBloc = false;
    bool finBloc = false;
    int acc=0;
    int debut=0;

    for(int i = 0; i < bloc.length; i++){
        if(i < (bloc.length - motCle.length -4) && bloc.substring(i,motCle.length+4) == "\""+motCle+"\":[" && !debutBloc){
            debut = i + motCle.length + 4;
            debutBloc = true;
        }
        if(debutBloc && !finBloc && bloc.substring(i,1)=="[") acc++;
        if(debutBloc && !finBloc && bloc.substring(i,1)=="]"){
            acc--;
            if(acc==0){
                finBloc = true;
                sousBloc = bloc.substring(debut,i-debut+1);
            }
        }
    }

    return sousBloc;
}

string lectureCle(string motCle, string bloc){

    string cle = "";
    bool debutCle = false;
    bool finCle = false;
    int debut=0;
    bool passe = false;

    for(int i = 0; i < bloc.length; i++){
        if(i < (bloc.length - motCle.length -2) && bloc.substring(i,motCle.length+2) == "\""+motCle+"\"" && !debutCle){
            debutCle = true;
            debut = i+motCle.length+4;
        }
        if(debutCle && !finCle && i>debut && bloc.substring(i,1)=="\"" && !passe){
            finCle = true;
            if(bloc.substring(debut-1,4) == "null"){
                cle ="null";
            }
            else{
                cle = bloc.substring(debut,i-debut);
            }
        }
        if(passe) passe = false;
        if(debutCle && !finCle && i>debut && bloc.substring(i,1)=="\\") passe = true;
    }

    cle = cle.replace("\\\"","\"").replace("\\r","").replace("\\n","\n");
    return cle;
}

string[] lectureVidURLs(string bloc){

    string[] vid = {};
    bool debutBloc = false;
    int acc=0;
    int debut=0;

    for(int i = 0; i < bloc.length; i++){
        if(debutBloc && bloc.substring(i,1)=="{") acc++;
        if(!debutBloc && bloc.substring(i,1)=="{"){
            acc++;
            debut = i+1;
            debutBloc = true;
        }
        if(debutBloc && bloc.substring(i,1)=="}"){
            acc--;
            if(acc==0){
                debutBloc = false;
                vid += lectureCle("url",bloc.substring(debut,i-debut+1));
                vid += lectureCle("title",bloc.substring(debut,i-debut+1));
            }
        }
    }
    return vid;
}

string analyseSource(string url){
    string source = "";

    if(url.substring(0,int.min(url.length,24))=="https://www.youtube.com/") source = "Youtube";

    return source;
}

string modifUrlYoutube(string url){
    // https://www.youtube.com/watch?v=XXXXXXXXXXX => https://www.youtube.com/embed/XXXXXXXXXXX
    int delimiteur = url.index_of_char('=');
    string numVideo = url.substring(delimiteur+1);
    string source = "https://www.youtube.com/embed/"+numVideo;
    return source;
}
}
