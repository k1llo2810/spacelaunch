namespace Spacelaunch{

public int calculDecompte(string lancement)
{
    int decompte;

    GLib.DateTime now = new GLib.DateTime.now_utc();

    //CRÉATION DATETIME LANCEMENT
    GLib.DateTime dateLancement = stringToDateTime(lancement);

    //CALCUL DIFFÉRENCE
    GLib.TimeSpan diff = dateLancement.difference(now);

    double difference = (double) diff;
    difference/=1E6;

    decompte = (int) difference;

    return decompte;
}

public string decompteToLabel(int decompte)
{
    string label = "";

    bool lancementEffectue = false;

    if(decompte < 0){
        decompte = -decompte;
        lancementEffectue = true;
    }

    int nbJours = decompte/86400;

    decompte-=nbJours*86400;

    int nbHeures = decompte/3600;

    decompte-=nbHeures*3600;

    int nbMinutes = decompte/60;

    decompte-=nbMinutes*60;

    string jours = nbJours.to_string();
    string heures = nbHeures.to_string();
    string minutes = nbMinutes.to_string();
    string secondes = decompte.to_string();

    if(jours.length==1) jours = "0"+jours;
    if(heures.length==1) heures = "0"+heures;
    if(minutes.length==1) minutes = "0"+minutes;
    if(secondes.length==1) secondes = "0"+secondes;

    if(lancementEffectue) label = "+ ";

    label += jours+":"+heures+":"+minutes+":"+secondes;
    return label;
}

public GLib.DateTime stringToDateTime(string lancement)
{
    GLib.TimeZone tz = new GLib.TimeZone.utc();
    int annee   = int.parse(lancement.substring(0,4));
    int mois    = int.parse(lancement.substring(5,2));
    int jour    = int.parse(lancement.substring(8,2));
    int heure   = int.parse(lancement.substring(11,2));
    int minute  = int.parse(lancement.substring(14,2));
    double seconde = double.parse(lancement.substring(17,2));
    GLib.DateTime dateLancement = new GLib.DateTime(tz,annee,mois,jour,heure,minute,seconde);

    return dateLancement;
}

public string dateTimeToString(GLib.DateTime dateLancement)
{
    string date = dateLancement.format("%d %B %Y");

    return date;
}
}
